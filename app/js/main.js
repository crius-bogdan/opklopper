(function (doc) {

  doc.addEventListener('DOMContentLoaded', function () {

    const dataset = 'dataset',
      clas = 'classList',
      activeClass = 'active',
      parentNode = 'parentNode',
      text = 'textContent',
      style = 'style',
      checked = 'checked',
      addClass = 'add',
      removeClass = 'remove';

    function match(element, className) {
      return element[clas].contains(className);
    }

    function getCoords(elem) {
      let box = elem.getBoundingClientRect();

      return box.top + pageYOffset;

    }

    const cooliesPolicy = doc.querySelector('.cookie');
    function smoothScroll(target) {
      if (target.hash !== '') {
        const hash = target.hash.replace("#", "")
        const link = doc.getElementsByName(hash);
        //Find the where you want to scroll
        const position = getCoords(link[0], true)
        let top = window.scrollY,
          num = 35;
        if (window.innerWidth < 768) {
          num = 75
        }

        let smooth = setInterval(() => {
          let leftover = position - top;
          // console.log();

          if (top === position) {
            clearInterval(smooth)
          } else if (position > top && leftover < num) {
            top += leftover
            window.scrollTo(0, top)
          } else if (position > (top - num) && leftover > 0) {
            top += num
            window.scrollTo(0, top)
          }
        }, 6)
      }
    }

    function getCookie(name) {
      let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options = {}) {

      options = {
        path: '/',
        ...options
      };

      if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
      }

      let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

      for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
          updatedCookie += "=" + optionValue;
        }
      }

      document.cookie = updatedCookie;
    }

    function checkCookie(name) {
      let checkCook = localStorage.getItem(name);

      if (!checkCook && cooliesPolicy) {
        cooliesPolicy[clas].remove('hide');
        return;
      } else {
        return;
      }
    }

    doc.querySelector('.cookie__close').addEventListener('click', () => {
      cooliesPolicy[clas].add('hide');
    });

    doc.querySelector('.cookie__btn').addEventListener('click', () => {
      setCookie('policy', 'true');
      localStorage.setItem('policy', 'true');
      cooliesPolicy[clas].add('hide');
    });

    checkCookie('policy');

    // if (questions) {
    //   doc.querySelector('.q-last')[text] = `0${questions.length}`;
    // }

    class SliderCards {
      constructor(elem, is_quiz) {
        this.is_quiz = is_quiz
        this.wrap = elem,
          this.elems = elem.querySelectorAll('[data-slider-cards]'),
          this.activeSlide = 0,
          this.slidersLendth = this.elems.length,
          this.spanCLast = elem.querySelector('.q-last'),
          this.spanCurrent = elem.querySelector('.q-current'),
          this.slidersQuanity = this.elems.length,
          this.currentSlide = this.elems[this.activeSlide]
      }

      init() {
        [...this.elems].forEach((el) => {
          if (el[dataset].id == this.activeSlide) {
            el[clas][addClass](activeClass)
          } else {
            el[clas][removeClass](activeClass)
          }
        })
        if (this.activeSlide == (this.slidersLendth - 1)) {
          this.wrap.querySelector('.btns-wrap')[clas][addClass]('hide')
        } else {
          this.wrap.querySelector('.btns-wrap')[clas][removeClass]('hide')
        }
        this.spanCLast[text] = `0${this.slidersQuanity}`
        this.spanCurrent[text] = `0${this.activeSlide + 1}`

        if(this.activeSlide + 1 === this.slidersQuanity && this.is_quiz) {
          doc.querySelector('.questions__wrap_bot #submitBtn').click();
          this.wrap.querySelector('.btns-wrap')[clas][addClass]('hide')
        }
      }

      next() {
        this.activeSlide = (this.activeSlide < (this.slidersQuanity - 1)) ? this.activeSlide += 1 : this.activeSlide = 0
        this.init();
      }

      prev() {
        this.activeSlide = (this.activeSlide < (this.slidersQuanity - 1) && this.activeSlide > 0) ? this.activeSlide -= 1 : this.activeSlide = 0
        this.init();
      }
    }

    class ProjectSlider {
      constructor(elem) {
        this.wrap = elem,
          this.elems = elem.querySelectorAll('[data-slide-project]'),
          this.activeSlide = 0,
          this.slidersLendth = this.elems.length,
          this.spanCLast = elem.querySelector('.q-last'),
          this.spanCurrent = elem.querySelector('.q-current'),
          this.slidersQuanity = this.elems.length,
          this.currentSlide = this.elems[this.activeSlide],
          this.progressBar = elem.querySelector('.projects__progress span')
      }

      init() {
        [...this.elems].forEach((el) => {
          console.log(el[dataset].id)
          if (el[dataset].id == this.activeSlide) {
            el[clas][addClass](activeClass)
          } else {
            el[clas][removeClass](activeClass)
          }
        })
        // if (this.activeSlide == (this.slidersLendth - 1)) {
        //   doc.querySelector('.btns-wrap')[clas][addClass]('hide')
        // } else {
        //   doc.querySelector('.btns-wrap')[clas][removeClass]('hide')
        // }
        // const percent =
        this.progressBar.style.width = `${((this.activeSlide + 1) * 100) / this.slidersQuanity}%`
        this.spanCLast[text] = `0${this.slidersQuanity}`
        this.spanCurrent[text] = `0${this.activeSlide + 1}`
      }

      next() {
        this.activeSlide = (this.activeSlide < (this.slidersQuanity - 1)) ? this.activeSlide += 1 : this.activeSlide = 0
        this.init();
      }

      prev() {
        this.activeSlide = (this.activeSlide < (this.slidersQuanity - 1) && this.activeSlide > 0) ? this.activeSlide -= 1 : this.activeSlide = 0
        this.init();
      }
    }

    let slider, sliderReview;

    if(doc.querySelector('.questions__wrap_bot')) {
      slider = new SliderCards(doc.querySelector('.questions__wrap_bot'), true);
      slider.init();
    }
    if(doc.querySelector('.review__wrap_bot')) {
      sliderReview = new SliderCards(doc.querySelector('.review__wrap_bot'));
      sliderReview.init()
    }
    // projectSlider.init()
    doc.addEventListener('click', (e) => {
      let target = e.target;
      // return;
      if (match(target, 'q-btn-prev')) {
        e.preventDefault()
        if (match(target, 'review__arr_left')) {
          sliderReview.prev();
          return
        }
        if (match(target, 'projects__arr_left')) {
          projectSlider.prev()
          return
        }
        slider.prev();
      }
      if (match(target, 'q-btn-next')) {
        e.preventDefault()
        if (match(target, 'review__arr_right')) {
          sliderReview.next();
          return
        }
        if (match(target, 'projects__arr_right')) {
          projectSlider.next()
          return
        }
        slider.next();
      }
      if (match(target, 'show-case')) {
        let case_id = target[dataset].id;
        const overlay = doc.querySelector('.case-over');
        const case_items = doc.querySelectorAll('.case-over-item');
        if (case_id) {
          for (let item of case_items) {
            let item_id = item[dataset].id;
            if (item_id === case_id) {
              overlay[clas][addClass](activeClass);
              document.body[clas][addClass]('hidden');
              item[clas][addClass](activeClass);
            } else {
              item[clas][removeClass](activeClass);
            }
          }
        }
      }
      if (match(target, 'close-case')) {
        doc.querySelector('.case-over')[clas][removeClass](activeClass);
        const case_items = doc.querySelectorAll('.case-over-item');
        document.body[clas][removeClass]('hidden');
        for (let item of case_items) {
          item[clas][removeClass](activeClass);
        }
      }
      if (match(target, 'menu-btn')) {
        doc.body[clas][addClass]('hidden');
        doc.querySelector('.nav__menu')[clas][addClass](activeClass);
      }
      if (match(target, 'menu-close') || match(target, 'menu-link') && window.innerWidth < 1100) {
        doc.body[clas][removeClass]('hidden');
        doc.querySelector('.nav__menu')[clas][removeClass](activeClass);
      }
      if (match(target, 'anchor') && window.innerWidth > 1100) {
        e.preventDefault();
        smoothScroll(target);
        return
      }

      // for (let itm of questions) {
      //   if (match(item, activeClass)) {
      //     for (let btn of questionSliderBtn) {
      //       if (match(btn, 'q-btn-prev') && match(target, 'q-btn-prev')) {
      //         target[dataset].prevId = itm[dataset].id - 1;
      //         target[dataset].nextId = itm[dataset].id;
      //       }
      //       if (match(btn, 'q-btn-next') && match(target, 'q-btn-next')) {

      //       }

      //     }
      //   }
      //   if(itm[dataset].id == targetId) {
      //     itm[clas][addClass](activeClass)
      //   } else {
      //     itm[clas][removeClass](activeClass)
      //   }
      // }
      // for (let i = 0; i < questions.length; i++) {
      //   if (questions[i][dataset].id == targetId)
      // }
      // }


    });


    svg4everybody({});

    class BlolbAnimation {
      constructor({wrap, blobRadius = 3, width = 900, height = 900, mass = 2000}) {
        this.two = new Two({
          type: Two.Types['svg'],
          width,
          height,
          autostart: true
        }).appendTo(wrap),
          // this.mass = 12,
          this.radius = this.two.height / blobRadius,
          this.strength = 0.0625,
          this.drag = 0.0,
          this.background = this.two.makeGroup(),
          this.foreground = this.two.makeGroup(),
          this.physics = new Physics(),
          this.points = [];
        for (let i = 0; i < Two.Resolution; i++) {
          this.pct = i / Two.Resolution;
          this.theta = this.pct * Math.PI * 2;
          this.ax = this.radius * Math.cos(this.theta);
          this.ay = this.radius * Math.sin(this.theta);
          this.variance = Math.random() * 0.5 + 0.5;
          this.bx = this.variance * this.ax;
          this.by = this.variance * this.ay;
          this.origin = this.physics.makeParticle(mass, this.ax, this.ay)
          this.particle = this.physics.makeParticle(Math.random() * mass * 0.66 + mass * 0.33, this.bx, this.by);
          this.spring = this.physics.makeSpring(this.particle, this.origin, this.strength, this.drag, 0);
          this.origin.makeFixed();
          this.particle.shape = this.two.makeCircle(this.particle.position.x, this.particle.position.y, 5);
          this.particle.shape.noStroke().fill = 'transparent';
          this.particle.position = this.particle.shape.translation;
          this.foreground.add(this.particle.shape)
          this.points.push(this.particle.position);
        }
        this.inner = new Two.Path(this.points, true, true)
        this.inner.noStroke();
        this.inner.scale = 1.2;
        this.background.add(this.inner);
        this.resize();
        this.two.bind('resize', this.resize).bind('update', () => {
          this.physics.update();
        })
      }

      resize() {
        this.background.translation.set(this.two.width / 2, this.two.height / 2);
        this.foreground.translation.set(this.two.width / 2, this.two.height / 2);
      }
    }

    const blobs = [
      {
        selector: '.header-sect .svg-sect',
        options: {}
      },
      {
        selector: '.about-sect .svg-sect',
        options: {}
      },
      {
        selector: '.adv-sect .svg-sect',
        options: {
          blobRadius: 3.4
        }
      },
      {
        selector: '.service-sec .svg-sect',
        options: {}
      },
      {
        selector: '.service-sec .svg-sect-two',
        options: {
          blobRadius: 3.4
        }
      },
      {
        selector: '.king-sect .svg-sect',
        options: {
          blobRadius: 2.8
        }
      },
      {
        selector: '.projects-sect .svg-sect',
        options: {
          blobRadius: 2.3
        }
      },
      {
        selector: '.questions-sect .svg-sect',
        options: {
          blobRadius: 2.7
        }
      },
      {
        selector: '.way-sect .svg-sect',
        options: {
          blobRadius: 2.8
        }
      },
      {
        selector: '.review-sect .svg-sect',
        options: {
          width: 1100,
          height: 1100,
          blobRadius: 2
        }
      },
      {
        selector: '.contact-sect .svg-sect',
        options: {
          blobRadius: 2.8
        }
      },
    ];

    blobs.forEach(blob => {
      let wrapper = doc.querySelector(blob.selector);
      if(wrapper) {
        new BlolbAnimation({wrap: wrapper, ...blob.options});
      }
    });

    // new BlolbAnimation({wrap: doc.querySelector('.header-sect .svg-sect')})
    // new BlolbAnimation({wrap: doc.querySelector('.about-sect .svg-sect')})
    // new BlolbAnimation({wrap: doc.querySelector('.adv-sect .svg-sect'), blobRadius: 3.4})
    // new BlolbAnimation({wrap: doc.querySelector('.service-sec .svg-sect')})
    // new BlolbAnimation({wrap: doc.querySelector('.service-sec .svg-sect-two'), blobRadius: 3.4})
    // new BlolbAnimation({wrap: doc.querySelector('.king-sect .svg-sect'), blobRadius: 2.8})
    // new BlolbAnimation({wrap: doc.querySelector('.projects-sect .svg-sect'), blobRadius: 2.3})
    // new BlolbAnimation({wrap: doc.querySelector('.questions-sect .svg-sect'), blobRadius: 2.7})
    // new BlolbAnimation({wrap: doc.querySelector('.way-sect .svg-sect'), blobRadius: 2.8})
    // new BlolbAnimation({wrap: doc.querySelector('.review-sect .svg-sect'), width: 1100, height: 1100, blobRadius: 2})
    // new BlolbAnimation({wrap: doc.querySelector('.contact-sect .svg-sect'), blobRadius: 2.8})
  });


})(document)
